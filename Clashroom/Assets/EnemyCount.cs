﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyCount : MonoBehaviour
{
     public int enemies;
    public Text enemyText;
    public string enemyString;

    private void Start()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemy").Length;
    }

    private void Update()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemy").Length;
        enemyString = enemies.ToString();
        enemyText.text = "Enemies Alive:" + enemyString;
    }
}
