﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrophyScript : MonoBehaviour
{

    public bool hit = false;
    PlayerHealth player;
    public ScoreUI scoreUI;
    public GameObject gm;
    public int score = 500;
    public AudioSource aud;

    private void Start()
    {
        gm = GameObject.Find("GameManager");
        scoreUI = gm.GetComponent<ScoreUI>();
        aud = GameObject.Find("Trophy").GetComponent<AudioSource>();
    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Debug.Log(hitInfo.name);
        player = hitInfo.GetComponent<PlayerHealth>();


        if (player != null)
        {
            hit = true;
        }

    }

    private void Update()
    {
        if (hit == true)
        {
            aud.Play();
            scoreUI.totalScore = scoreUI.totalScore + score;
            Destroy(gameObject);
        }
    }
}
