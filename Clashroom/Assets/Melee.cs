﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee : MonoBehaviour {

    public Transform firePoint;
    public Transform firePoint2;
    public float meleeRange;
    public LayerMask enemies;
    public AudioSource aud;
    public bool attack;
    public bool test;
    PlayerHealth playerHealth;
    bool cooldown;

    float wait = 0.5f;

    public Animator animator;

    private float searchCountdown = 0.5f;


    private void Start()
    {
        aud = GameObject.Find("Woosh").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {

            aud.Play();
            StartCoroutine(MeleeAnim());
            
        }

        if (DoDamage())
        {

            if (attack == true)
            {
                Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(firePoint.position, meleeRange, enemies);
                for (int i = 0; i < enemiesToDamage.Length; i++)
                {
                    if (cooldown == false)
                    {
                        enemiesToDamage[i].GetComponent<Enemy>().TakeDamage(80);
                        cooldown = true;
                        StartCoroutine(CoolDown());
                    }
                    
                    
                }
            }



        }

    }

    IEnumerator MeleeAnim()
    {
        attack = true;
        animator.SetBool("IsMelee", true);
        yield return new WaitForSeconds(wait);
        animator.SetBool("IsMelee", false);
        attack = false;
        yield return null;
    }

    

    public bool DoDamage()
    {
        searchCountdown -= Time.deltaTime;
        if (searchCountdown <= 0)
        {
           
                Debug.Log ("attack");
                return true;
                
            
        }
        Debug.Log("stopped");
        return false;
    }

   
    IEnumerator CoolDown()
    {

        yield return new WaitForSeconds(0.3f);
        cooldown = false;
        yield return null;


    }


}
