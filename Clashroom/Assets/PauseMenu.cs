﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public bool gameIsPaused = false;
    public GameObject pauseMenuUI;
    GameObject player;
    Weapon weapon;
    PlayerHealth health;
    PlayerMovement movement;
    Melee melee;

    private void Start()
    {
        player = GameObject.Find("Player");
        weapon = player.GetComponent<Weapon>();
        health = player.GetComponent<PlayerHealth>();
        movement = player.GetComponent<PlayerMovement>();
        melee = player.GetComponent<Melee>();
    }


    void Update ()
    {
		if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameIsPaused)
            {
                Resume();
               
            }
            else
            {
                Pause();
               
            }
        }
	}

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
        weapon.enabled = true;
        movement.enabled = true;
        melee.enabled = true;
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
        weapon.enabled = false;
        movement.enabled = false;
        melee.enabled = false;
    }
    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void Retry()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1f;
        gameIsPaused = false;
        weapon.enabled = true;
        movement.enabled = true;
        melee.enabled = true;
        health.playerHealth = 100f;
    }
}

