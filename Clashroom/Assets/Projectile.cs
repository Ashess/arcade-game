﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public float speed = 20f;
    public Rigidbody2D rb;
    AudioSource splash;
    public GameObject splashFX;
    public GameObject splashFlip;
    
	
	void Start ()
    {
        
        splash = GameObject.Find("Splash_Impact").GetComponent<AudioSource>();
        rb.velocity = transform.right * speed;

	}

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Debug.Log(hitInfo.name);
        Enemy enemy = hitInfo.GetComponent<Enemy>();
        if (enemy != null)
        {
            enemy.TakeDamage(50);
            splash.Play();
            Instantiate(splashFX, transform.position, Quaternion.identity);
            Destroy(gameObject);
            Instantiate(splashFlip, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
        if (hitInfo.tag == "Map")
        {

            Instantiate(splashFX, transform.position, Quaternion.identity);
            Destroy(gameObject);
            Instantiate(splashFlip, transform.position, Quaternion.identity);
            Destroy(gameObject);

        }
    }
}
