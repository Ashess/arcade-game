﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Noise : MonoBehaviour
{

    public AudioSource laugh;
    private float randomTime;
    private float timeCounter = 0;


    private void Start()
    {
        randomTime = Random.Range(15, 30);
    }

    void Update ()
    {
        if (timeCounter > randomTime)
        {
            randomTime = Random.Range(15, 30);
            timeCounter = 0;
            laugh.Play();
        }

        timeCounter += Time.deltaTime;
    }
}
