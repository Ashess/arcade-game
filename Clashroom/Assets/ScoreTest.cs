﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTest : MonoBehaviour {

    public bool playerEntered = false;
    public ScoreUI scoreUI;
    
    

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {

            playerEntered = true;


        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {

            playerEntered = false;


        }
    }

    private void Update()
    {
        if(playerEntered == true)
        {

            scoreUI.totalScore = scoreUI.totalScore + 100;
            Destroy(gameObject);
        }
    }
}
