﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour {

    public string health;
    public int currentHealth;
    public Text healthText;
    GameObject player;
    PlayerHealth healthScript;

    private void Start()
    {
        player = GameObject.Find ("Player");
        healthScript = player.GetComponent<PlayerHealth>();
        healthText.text = "Health:" + health;
    }

    private void Update()
    {
        health =healthScript.playerHealth.ToString();
        //health = currentHealth.ToString();
        healthText.text = "Health:" + health;

        if (healthScript.playerHealth <= 0)
        {
            healthText.text = "Health: 0";
        }
    }

}
