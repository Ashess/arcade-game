﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAddSpawn : MonoBehaviour {

    public Transform enemy;
    GameObject boss;

    private void Start()
    {
        boss = GameObject.Find("EnemyBoss");
        StartCoroutine(adSpawn());
    }

    private void Update()
    {
        Transform bossLocation = boss.transform;
    }

    IEnumerator adSpawn()
    {
        Transform bossLocation = boss.transform;
        Instantiate(enemy, bossLocation);


        yield break;
    }
}
