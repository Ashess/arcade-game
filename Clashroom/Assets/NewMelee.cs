﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewMelee : MonoBehaviour
{

    public float meleeRange;
    public LayerMask enemies;
    public Transform firePoint;
    bool cooldown;

    private void Update()
    {

        if (cooldown == false)
        {
            Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(firePoint.position, meleeRange, enemies);
            for (int i = 0; i < enemiesToDamage.Length; i++)
            {

                enemiesToDamage[i].GetComponent<Enemy>().TakeDamage(80);
                cooldown = true;
               

            }
        }
        


    }

    IEnumerator CoolDown()
    {
       
        yield return new WaitForSeconds(1);
        cooldown = false;
        yield return null;
        

    }
}
