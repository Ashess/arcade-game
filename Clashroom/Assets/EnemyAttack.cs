﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour {

    public float damage = 20f;
    GameObject player;
    PlayerHealth healthScript;

    private void Start()
    {
        player = GameObject.Find("Player");
        healthScript = player.GetComponent<PlayerHealth>();
    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Debug.Log(hitInfo.name);
        PlayerHealth player = hitInfo.GetComponent<PlayerHealth>();

        if (player != null)
        {
            Debug.Log("Enemy has hit player");
            healthScript.playerHealth = healthScript.playerHealth - damage;
        }

    }
}
