﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

[RequireComponent(typeof (Rigidbody2D))]
[RequireComponent (typeof (Seeker))]
public class EnemyAI : MonoBehaviour {

    //What to chase
    public Transform target;
    //How many times each second we will update the path
    public float updateRate = 2f;

    private Seeker seeker;
    private Rigidbody2D rb;

    //The calculatede path
    public Path path;

    //The AI speed per second
    public float speed = 300;
    public ForceMode2D fMode;

    [HideInInspector]
    public bool pathIsEnded = false;

    //The Max distance from an AI to a waypoint for it to continue to the next waypoint
    public float nextWaypointDistance = 3;

    //They Waypoint we are currently moving towards
    private int currentWaypoint = 0;

    void Start()
    {
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();

        if (target == null)
        {
            Debug.LogError("No player found? Shit Onesself");
        }
        // Start a new path to the target position, return the result to the OnPathComplete method
        seeker.StartPath(transform.position, target.position, OnPathComplete);

        StartCoroutine(UpdatePath());
    }

   IEnumerator UpdatePath()
   {
        if (target == null)
        {
            //TODO: Insert a player search here.
            
            yield return false;
        }
        // Start a new path to the target position, return the result to the OnPathComplete method
        seeker.StartPath(transform.position, target.position, OnPathComplete);
        

        yield return new WaitForSeconds(1f / updateRate);
        StartCoroutine(UpdatePath());
   }

    public void OnPathComplete(Path p)
    {
        Debug.Log("We got a path, did it have an error?" + p.error);
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    private void FixedUpdate()
    {
        StartCoroutine(thisStuff());
    }

     IEnumerator thisStuff()
     {
         {
             if (target == null)
             {
                //TODO: Insert a player search here.
                
                yield return false;
             }
             //TODO: Always Looks at player
             if (path == null)
             {
                 yield break;
             }

             if (currentWaypoint >= path.vectorPath.Count)
             {
                 if (pathIsEnded)
                 {
                     yield break;
                 }
                 Debug.Log("End of path reached.");
                 pathIsEnded = true;
                 yield break;
             }

             pathIsEnded = false;

             //Direction to the next waypoint
             Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
             dir *= speed * Time.fixedDeltaTime;

             //Move the AI
             rb.AddForce(dir, fMode);

             float dist = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
             if (dist < nextWaypointDistance)
             {
                 currentWaypoint++;
                 yield break;
             }
         }

     }
    /*  if (target == null)
           {
               //TODO: search for player

           }

           //TODO: Always look at player?

           if (path == null)
           {
               //TODO: Look at player
           }

           if (currentWaypoint >= path.vectorPath.Count)
           {
               if (pathIsEnded)
               {

                   Debug.Log("End of path reached");
                   pathIsEnded = true;
               }

           }
           pathIsEnded = false;

           //Direction to the next waypoint
           Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
           dir *= speed * Time.fixedDeltaTime;

           //move the AI
           rb.AddForce(dir, fMode);

           float dist = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
           if (dist < nextWaypointDistance)
           {
               currentWaypoint++;

           } */
}


