﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownTeleport : MonoBehaviour {

    public Transform target;
    public GameObject player;
    public bool playerEntered = false;
    public Transform ducks;

    void Start()
    {
        ducks.GetComponent<ParticleSystem>().enableEmission = false;
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {

            playerEntered = true;

        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {

            playerEntered = false;


        }
    }
    private void Update()
    {
        if (playerEntered == true)
        {
            if (Input.GetKeyDown("s"))
            {
                player.transform.position = target.position;
                Debug.Log("test");
                ducks.GetComponent<ParticleSystem>().enableEmission = true;
                StartCoroutine(stopDucks());
            }
        }
        if (playerEntered == true)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                player.transform.position = target.position;
                Debug.Log("test");
                ducks.GetComponent<ParticleSystem>().enableEmission = true;
                StartCoroutine(stopDucks());
            }
        }
    }
    IEnumerator stopDucks()
    {
        yield return new WaitForSeconds(.8f);
        ducks.GetComponent<ParticleSystem>().enableEmission = false;
    }
}

