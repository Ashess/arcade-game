﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUI : MonoBehaviour {

    public string total;
    public int totalScore;
    public Text score;
    public Text deathScore;
    public Text endScore;

    private void Start()
    {
        score.text = "Score:" + total;
    }

    private void Update()
    {
        total = totalScore.ToString();
        score.text = "Score: " + total;
        deathScore.text = "Score: " + total;
        endScore.text = "Final Score: " + total;
    }
}
