﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour {

    public float playerHealth = 100f;
    public GameObject deathMenu;

    GameObject player;
    Weapon weapon;
    PlayerMovement movement;
    Melee melee;

    public AudioSource oof;

    private void Start()
    {
        player = GameObject.Find("Player");
        weapon = player.GetComponent<Weapon>();
        movement = player.GetComponent<PlayerMovement>();
        melee = player.GetComponent<Melee>();
    }



    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Debug.Log(hitInfo.name);
        Enemy enemy = hitInfo.GetComponent<Enemy>();

        if (enemy != null)
        {
            playerHealth = playerHealth - enemy.damage;
            oof.Play();
            Debug.Log(playerHealth);
        }

    }


    private void Update()
    {
        if (playerHealth <= 0)
        {
            Debug.Log("You are dead");
            Death();
        }
    }
    void Death()
    {
        
        deathMenu.SetActive(true);
        Time.timeScale = 0f;
        weapon.enabled = false;
        movement.enabled = false;
        melee.enabled = false;

        //playerHealth = 100f;
    }
    
    
}
