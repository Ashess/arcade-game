﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : MonoBehaviour 
{

    public float healAmount = 20f;
    public bool hit = false;
    PlayerHealth player;
    public AudioSource aud;


    private void Start()
    {
        aud = GameObject.Find("Chomp").GetComponent<AudioSource>();
    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Debug.Log(hitInfo.name);
        player = hitInfo.GetComponent<PlayerHealth>();
        

        if (player != null)
        {
            hit = true;
        }

    }

    private void Update()
    {

        if (hit == true)
        {
            if (player.playerHealth < 100)
            {
                GetComponent<BoxCollider2D>().enabled = false;
                aud.Play();
                Destroy(gameObject);
                player.playerHealth = player.playerHealth + healAmount;

            }
        }
      
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        hit = false;
    }
}
