﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public int health = 100;
    public int score = 100;
    public Rigidbody2D rb;
    public ScoreUI scoreUI;
    public GameObject gm;
    public float damage;
    public GameObject splatter;
    public AudioSource aud;
    public AudioSource mel;
    public GameObject WindFX;
    float knockback = 10f;

    // public GameObject deathEffect;

    private void Start()
    {
        gm = GameObject.Find("GameManager");
        scoreUI = gm.GetComponent<ScoreUI>();
        aud = GameObject.Find("Enemy Death").GetComponent<AudioSource>();
        mel = GameObject.Find("Whack").GetComponent<AudioSource>();

    }



    public void TakeDamage(int damage)
    {
        health -= damage;
        
        if (health <= 0)
        {
            Die();
            scoreUI.totalScore = scoreUI.totalScore + score;
        }
        if( damage == 80)
        {
            Instantiate(WindFX, transform.position, Quaternion.identity);
            mel.Play();
            
        }
    }
    public void Die()
    {

        aud.Play();
        Instantiate(splatter, transform.position, Quaternion.identity);

        //Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);

    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Debug.Log(hitInfo.name);
        CharacterController2D player = hitInfo.GetComponent<CharacterController2D>();

        if (player != null)
        {
            Die();
        }

    }

    
}
