﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIFollowPlayer : MonoBehaviour {

    GameObject Player;
    public float speed = 3f;
    public float MaxDist = 10f;
    public float MinDist = 0f;
    public AIRandomMovement random;

    void Start()
    {
        Player = GameObject.Find("Player");
    }

    void Update()
    {
       



        if (Vector2.Distance(transform.position, Player.transform.position) <= MaxDist)
        {
            random.enabled = false;
            transform.position = Vector2.MoveTowards(transform.position, Player.transform.position, speed * Time.deltaTime);
        }
        else
        {

            random.enabled = true;
        }

       
    }
}
